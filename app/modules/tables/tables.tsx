import './tables.scss';

import React from 'react';
import { Row, Col, Card } from 'reactstrap';
import { Dropdown, Button, DropdownTag, TableHeader, Table, Pagination, ICurrentPageData } from 'lib/components';
import MoreVert from '@material-ui/icons/MoreVert';
import SaveAltRounded from '@material-ui/icons/SaveAltRounded';
import { translateItem } from 'lib/util';

const exampleData1 = [
  ['Account Balances', '1 Lorem ipsum doret samet consequat electur…'],
  ['Accounts on Notice', '2 Lorem ipsum doret samet consequat electur…'],
  ['Agent Capital Balances', '3 Lorem ipsum doret samet consequat electur…']
];

const exampleData2 = [];

for (let i = 0; i < 100; i++) {
  exampleData2.push([`Title ${i + 1}`, `${i + 1} Lorem ipsum doret samet consequat electur…`]);
}

const renderHeader = data => (
  <tr>
    <TableHeader sort={'unsorted'}>{translateItem('global.table.headers.name')}</TableHeader>
    <TableHeader>{translateItem('global.table.headers.description')}</TableHeader>
    <TableHeader sort={'ascending'}>Tag</TableHeader>
    <th />
    <th />
    <th />
    <th />
  </tr>
);

const renderRow = (item: any) => (
  <tr>
    <th scope="row">{item[0]}</th>
    <td>{item[1]}</td>
    <td>
      <div style={{ display: 'flex', flexDirection: 'row' }}>
        <DropdownTag selectable value={{ value: 1, name: 'Tag1', display: 'Tag 1', selected: false }} />
        <DropdownTag selectable value={{ value: 2, name: 'Tag2', display: 'Tag 2', selected: true }} />
      </div>
    </td>
    <td>
      <Button color="primary">button</Button>
    </td>
    <td>
      <Dropdown placeholder="Actions" />
    </td>
    <td>
      <SaveAltRounded />
    </td>
    <td>
      <MoreVert />
    </td>
  </tr>
);

const renderTablePage = (currentPage: ICurrentPageData) => (
  <Table renderRow={renderRow} renderHeader={renderHeader} tableRowData={currentPage.data} />
);

export class Tables extends React.Component {
  onPageChange = (selectedPage: number, offset: number, perPage: number) => {};
  render() {
    return (
      <>
        <div className="small-header">Simple Table</div>

        <Card>
          <Row>
            <Col>
              <Table renderRow={renderRow} renderHeader={renderHeader} tableRowData={exampleData1} />
            </Col>
          </Row>
        </Card>

        <div className="small-header">Paginated Table</div>

        <Card>
          <Row>
            <Col>
              <Pagination
                perPageDefault={20}
                data={exampleData2}
                render={renderTablePage}
                justify="end"
                showPerPage
                onPageChange={this.onPageChange}
              />
            </Col>
          </Row>
        </Card>
      </>
    );
  }
}

import './forms.scss';

import React from 'react';
import { Input, FormText, FormFeedback, FormGroup, Label, Card } from 'reactstrap';

import {
  TagInput,
  SearchBar,
  TextInput,
  RadioInput,
  ComboboxInput,
  IValueDirtyAndValid,
  iformInput,
  MultipleSelectionInput,
  Button,
  CalendarInput,
  DropdownSearchBar,
  Container,
  FileUpload,
  Col,
  Row,
  wrapChanges,
  IFormInput
} from 'lib/components';
import { ITranslatedSelectableValue, translatedValue } from 'lib/util';
import { IValidateAndI18nKey, required, requiredString, stringIsNumber, validationErrors } from 'lib/validation';

import PersonOutline from '@material-ui/icons/PersonOutline';
import Info from '@material-ui/icons/Info';
import Cancel from '@material-ui/icons/Cancel';
import CheckCircleRoundedIcon from '@material-ui/icons/CheckCircleRounded';

interface IsSmartAnimal {
  name: string;
  smart: boolean;
}

export interface IFormsState {
  tagValues: Array<ITranslatedSelectableValue<string>>;
  search?: string;
  numberInput: string;
  numberInputDirty: boolean;
  numberInputDisabled: boolean;
  textEmpty: IValueDirtyAndValid<string>;
  textValue: IValueDirtyAndValid<string>;
  textError: IValueDirtyAndValid<string>;
  textSuccess: IValueDirtyAndValid<string>;
  textRequired1: string;
  textRequired2: string;
  radInput: string;
  radDirty: boolean;
  radDisabled: boolean;
  comboVal?: IValueDirtyAndValid<IsSmartAnimal>;
  comboDisabled: boolean;
  multipleVal?: IValueDirtyAndValid<string[]>;
  multipleDisabled: boolean;
  comboChoices: () => Map<IsSmartAnimal, string>;
  multiChoices: () => Map<string, string>;
  date: Date;
}

const defaultValues: IFormsState = {
  tagValues: [
    { value: 'tag1', display: 'tag1', selected: false },
    { value: 'tag2', display: 'tag2', selected: true },
    { value: 'tag3', display: 'tag3', selected: true }
  ],
  search: '',
  numberInput: '',
  textEmpty: { value: '' },
  textValue: { value: 'Value', validation: [] },
  textError: {
    value: 'Error',
    validation: [{ func: requiredString, i18n: 'Value is required' }, { func: stringIsNumber, i18n: 'Must be a number' }]
  },
  textSuccess: { value: 'Success', validation: [{ func: requiredString, i18n: 'Value is required' }] },
  textRequired1: '',
  textRequired2: '',
  numberInputDirty: false,
  numberInputDisabled: false,
  radInput: '',
  radDirty: false,
  comboVal: { value: undefined },
  multipleVal: { value: undefined },
  comboChoices: () =>
    new Map([
      [{ name: 'sheep', smart: false }, 'Sheep'],
      [{ name: 'dog', smart: true }, 'Dog'],
      [{ name: 'horse', smart: true }, 'Horse'],
      [{ name: 'cow', smart: false }, 'Cow']
    ]),
  multiChoices: () => new Map<string, string>([['one', 'One'], ['two', 'Two'], ['three', 'Three']]),
  radDisabled: false,
  comboDisabled: false,
  multipleDisabled: false,
  date: null
};

// React.Component<IFormInput<IRelatedPartyOtherDetails>, IRelatedPartyOtherDetails> {
export class Forms extends React.Component<IFormInput<IFormsState>, IFormsState> {
  updateCount = 1;

  constructor(props) {
    super(props);
    this.onAddTag = this.onAddTag.bind(this);
    this.searchChanged = this.searchChanged.bind(this);
    this.state = !!this.props.value ? this.props.value : defaultValues;
  }

  onAddTag(a: ITranslatedSelectableValue<string>) {
    if (!!a) {
      this.setState(p => ({
        tagValues: [...p.tagValues, a]
      }));
    }
  }

  searchChanged(search: string) {
    this.setState({ search });
  }

  handleDateChange = chosenDate => {
    this.setState({
      date: chosenDate
    });
  };

  renderRadioButtonInput() {
    const choices = () =>
      new Map<string, string>([['Yes', 'forms.radioInput.yes'], ['No', 'forms.radioInput.no'], ['Maybe', 'forms.radioInput.maybe']]);
    const onChange = radInput => this.setState({ radInput });
    const onDirty = () => this.setState({ radDirty: true });
    const notMaybe: IValidateAndI18nKey<string> = {
      func: (i18nKey, val) => (val !== 'Maybe' ? [] : [translatedValue('Maybe you should choose')]),
      i18n: ''
    };
    const notNo: IValidateAndI18nKey<string> = {
      func: (i18nKey, val) => (val !== 'No' ? [] : [translatedValue('Not taking no for an answer')]),
      i18n: ''
    };

    const toggleDisabled = () => this.setState(p => ({ radDisabled: !p.radDisabled }));

    return (
      <Row>
        <Col md="12">
          <div className="small-header">Using RadioInput component and validations</div>
        </Col>
        <Col md="4">
          <RadioInput
            md={4}
            choices={choices}
            id="radInput"
            value={this.state.radInput}
            selected="No"
            onChange={onChange}
            dirty={this.state.radDirty}
            onMadeDirty={onDirty}
            label="Do you want coffee?"
            helpMessage="Maybe is not allowed"
            validMessage="Some coffee on its way"
            validation={[notMaybe, notNo]}
            disabled={this.state.radDisabled}
          />
          <Button onClick={toggleDisabled}>{this.state.radDisabled ? 'Enable' : 'Disable'}</Button>
        </Col>
      </Row>
    );
  }

  get incrementUpdateCount(): number {
    return ++this.updateCount;
  }

  renderComboboxInput() {
    const comboVals = iformInput(a => a.comboVal, this);

    const isSmart: IValidateAndI18nKey<IsSmartAnimal> = {
      func: (i18nKey: string, v: IsSmartAnimal) => (!!v && v.smart ? [] : [translatedValue<IsSmartAnimal>('Not a smart animal')]),
      i18n: ''
    };
    const onClick = () =>
      this.setState({
        comboChoices: () =>
          new Map([
            [{ name: 'sheep', smart: false }, `Sheep ${this.incrementUpdateCount}`],
            [{ name: 'dog', smart: true }, `Dog ${this.updateCount}`],
            [{ name: 'horse', smart: true }, `Horse ${this.updateCount}`],
            [{ name: 'cow', smart: false }, `Cow ${this.updateCount}`]
          ])
      });
    const toggleDisabled = () => this.setState(p => ({ comboDisabled: !p.comboDisabled }));
    return (
      <Row>
        <Col md="12">
          <div className="small-header">Using ComboInput component and validations</div>
        </Col>
        <Col md="4">
          <ComboboxInput
            choices={this.state.comboChoices}
            id="smartAnimal"
            {...comboVals}
            validation={[isSmart]}
            label="Label for ComboboxInput"
            helpMessage="Try to select a smart animal"
            placeholder="forms.comboInput.placeholder"
            required
            disabled={this.state.comboDisabled}
          />
          <div style={{ display: 'flex', flexDirection: 'row' }}>
            <Button style={{ marginRight: '8px' }} onClick={onClick} color="primary">
              Simulate update
            </Button>
            <Button onClick={toggleDisabled}>{this.state.comboDisabled ? 'Enable' : 'Disable'}</Button>
          </div>
        </Col>
      </Row>
    );
  }

  renderMultipleSelectionInput() {
    const only2: IValidateAndI18nKey<string[]> = {
      func: (k, v) => (!!v && v.length === 2 ? [] : [translatedValue<string[]>('Must select exactly two')]),
      i18n: 'Must select exactly two'
    };
    const requiredVal = { func: required, i18n: 'Please select a value' };
    const vals = iformInput(b => b.multipleVal, this, [requiredVal, only2]);
    const onClick = () =>
      this.setState({ multiChoices: () => new Map<string, string>([['one', 'One 1'], ['two', 'Two 2'], ['three', 'Three 3']]) });

    const toggleDisabled = () => this.setState(p => ({ multipleDisabled: !p.multipleDisabled }));

    return (
      <Row>
        <Col md="12">
          <div className="small-header">Multiple selections with selection bar</div>
        </Col>
        <Col md="4">
          <MultipleSelectionInput
            label="Select any 2"
            {...vals}
            choices={this.state.multiChoices}
            selected={['one', 'two']}
            selectionBar
            disabled={this.state.multipleDisabled}
          />
          <div style={{ display: 'flex', flexDirection: 'row' }}>
            <Button style={{ marginRight: '8px' }} onClick={onClick} color="primary">
              Simulate change
            </Button>
            <Button onClick={toggleDisabled}>{this.state.multipleDisabled ? 'Enable' : 'Disable'}</Button>
          </div>
        </Col>
      </Row>
    );
  }

  renderTextInputFields = () => {
    const validate = () => ![this.state.textError, this.state.textSuccess].some(v => validationErrors(v.value, v.validation).length > 0);
    const change = () => this.state;

    const textEmpty = wrapChanges(iformInput(a => a.textEmpty, this), this.props, null, change);
    const textValue = wrapChanges(iformInput(a => a.textValue, this), this.props, null, change);
    const textError = wrapChanges(iformInput(a => a.textError, this), this.props, validate, change);
    const textSuccess = wrapChanges(iformInput(a => a.textSuccess, this), this.props, validate, change);
    return (
      <>
        <Row>
          <Col>
            <div className="small-header">Text inputs no Icon</div>

            <TextInput placeholder="Placeholder" id="inputEmpty" {...textEmpty} />
            <TextInput placeholder="Placeholder" id="inputWithValue" {...textValue} />
            <TextInput placeholder="Placeholder" id="inputError" {...textError} enableTicks dirty />
            <TextInput placeholder="Placeholder" id="inputSuccess" enableTicks {...textSuccess} dirty />
            <TextInput placeholder="Placeholder" id="inputWithValue" {...textValue} value="Read only/Disabled" disabled />
          </Col>
          <Col>
            <div className="small-header">Text inputs Mandatory</div>
            <TextInput placeholder="Placeholder" id="inputEmpty2" {...textEmpty} required />
            <TextInput placeholder="Placeholder" id="inputWithValue2" {...textValue} required />
            <TextInput placeholder="Placeholder" id="inputError2" {...textError} enableTicks dirty required />
            <TextInput placeholder="Placeholder" id="inputSuccess2" enableTicks {...textSuccess} dirty required />
            <TextInput placeholder="Placeholder" id="inputWithValue2" {...textValue} disabled value="Read only/Disabled" required />
          </Col>
        </Row>
      </>
    );
  };

  renderTextInputFieldsWidthIcons = () => {
    const validate = () => ![this.state.textError, this.state.textSuccess].some(v => validationErrors(v.value, v.validation).length > 0);
    const change = () => this.state;

    const textEmpty = wrapChanges(iformInput(a => a.textEmpty, this), this.props, null, change);
    const textValue = wrapChanges(iformInput(a => a.textValue, this), this.props, null, change);
    const textError = wrapChanges(iformInput(a => a.textError, this), this.props, validate, change);
    const textSuccess = wrapChanges(iformInput(a => a.textSuccess, this), this.props, validate, change);
    return (
      <>
        <Row>
          <Col>
            <div className="small-header">Icon on the left</div>

            <TextInput placeholder="Placeholder" id="inputEmpty" {...textEmpty} iconLeft={<PersonOutline />} />
            <TextInput placeholder="Placeholder" id="inputWithValue" {...textValue} iconLeft={<PersonOutline />} />
            <TextInput placeholder="Placeholder" id="inputError" {...textError} enableTicks dirty iconLeft={<PersonOutline />} />
            <TextInput placeholder="Placeholder" id="inputSuccess" enableTicks {...textSuccess} dirty iconLeft={<PersonOutline />} />
            <TextInput
              placeholder="Placeholder"
              id="inputWithValue"
              {...textValue}
              disabled
              value="Read only/Disabled"
              iconLeft={<PersonOutline />}
            />
          </Col>
          <Col>
            <div className="small-header">Icon on the right</div>
            <TextInput placeholder="Placeholder" id="inputEmpty2" {...textEmpty} required iconRight={<Info />} />
            <TextInput placeholder="Placeholder" id="inputWithValue2" {...textValue} required iconRight={<Info />} />
            <TextInput placeholder="Placeholder" id="inputError2" {...textError} enableTicks dirty required iconRight={<Info />} />
            <TextInput placeholder="Placeholder" id="inputSuccess2" enableTicks {...textSuccess} dirty required iconRight={<Info />} />
            <TextInput
              placeholder="Placeholder"
              id="inputWithValue2"
              {...textValue}
              disabled
              value="Read only/Disabled"
              required
              iconRight={<Info />}
            />
          </Col>
        </Row>
      </>
    );
  };

  renderTextInputWithLabel = () => {
    const validate = () => ![this.state.textError, this.state.textSuccess].some(v => validationErrors(v.value, v.validation).length > 0);
    const change = () => this.state;

    const textEmpty = wrapChanges(iformInput(a => a.textEmpty, this), this.props, null, change);
    const textValue = wrapChanges(iformInput(a => a.textValue, this), this.props, null, change);
    const textError = wrapChanges(iformInput(a => a.textError, this), this.props, validate, change);
    const textSuccess = wrapChanges(iformInput(a => a.textSuccess, this), this.props, validate, change);
    return (
      <>
        <Row>
          <Col>
            <div className="small-header">Icon on the right</div>
            <TextInput label="Label" placeholder="Placeholder" id="inputEmpty2" {...textEmpty} required />
            <TextInput label="Label" placeholder="Placeholder" id="inputWithValue2" {...textValue} required helpMessage="Help Message" />
            <TextInput label="Label" placeholder="Placeholder" id="inputError2" {...textError} enableTicks dirty required />
            <TextInput
              label="Label"
              placeholder="Placeholder"
              id="inputSuccess2"
              enableTicks
              {...textSuccess}
              validMessage="Positive Message"
              dirty
              required
            />
            <TextInput
              label="Label"
              placeholder="Placeholder"
              id="inputWithValue2"
              {...textValue}
              disabled
              required
              value="Read only/Disabled"
            />
          </Col>
        </Row>
      </>
    );
  };

  onSelectAllTags = () => {
    const { tagValues } = this.state;
    tagValues.map(item => (item.selected = true));
    this.setState({ tagValues });
  };

  render() {
    return (
      <>
        <Card>
          {this.renderTextInputFields()}
          {this.renderTextInputFieldsWidthIcons()}
          {this.renderTextInputWithLabel()}
          {this.renderRadioButtonInput()}
          {this.renderComboboxInput()}
          {this.renderMultipleSelectionInput()}
          <Row>
            <Col md="12">
              <div className="small-header">Input Bars</div>
            </Col>
            <Col md="8">
              <Label>SearchBar</Label>
              <SearchBar value={this.state.search} onSearchChanged={this.searchChanged} />
              <FormFeedback valid>
                <Info className="material-icons" />
                {this.state.search}
              </FormFeedback>
            </Col>
          </Row>
          <Row>
            <Col md="8">
              <Label>SearchBar (disabled)</Label>
              <SearchBar disabled onSearchChanged={this.searchChanged} />
              <FormFeedback valid>
                <Info className="material-icons" />
                {this.state.search}
              </FormFeedback>
            </Col>
          </Row>
          <Row>
            <Col md="8">
              <Label>SearchBar Global</Label>
              <SearchBar global onSearchChanged={this.searchChanged} />
            </Col>
          </Row>
          <Row>
            <Col md="12">
              <div className="small-header">Async dropdown selection (TagInput)</div>
              <Button onClick={this.onSelectAllTags}>Select all tags</Button>
            </Col>
            <Col md="6">
              <TagInput label="Tag Input with dropdown" placeholder="placeholder" values={this.state.tagValues} onAddTag={this.onAddTag} />
            </Col>
          </Row>
          <Row>
            <Col>
              <FormGroup>
                <CalendarInput
                  placeholder="Start Date"
                  label="Label For Start Date Calendar required"
                  id="expiry-date-cal"
                  value={this.state.date}
                  onChange={this.handleDateChange}
                  required
                />
              </FormGroup>
            </Col>
            <Col>
              <FormGroup>
                <CalendarInput
                  placeholder="End Date"
                  label="Label For End Date Calendar"
                  id="expiry-date-cal"
                  value={this.state.date}
                  onChange={this.handleDateChange}
                />
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col>
              <FormGroup>
                <Label>CalendarInput (disabled)</Label>
                <CalendarInput id="expiry-date-cal-dis" required disabled value={this.state.date} onChange={this.handleDateChange} />
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col>
              <Label>DropdownSearchBar (controlled)</Label>
              <DropdownSearchBar
                value={this.state.search}
                id="dropdown-search-bar-1"
                placeholder="Type something"
                onSearchChanged={this.searchChanged}
              >
                <Container>
                  <Row>
                    <Col>
                      Custom content goes here:
                      <Button>Button</Button>
                      <CalendarInput id="expiry-date-cal-2" value={this.state.date} onChange={this.handleDateChange} />
                    </Col>
                  </Row>
                </Container>
              </DropdownSearchBar>
            </Col>
          </Row>
          <Row>
            <Col>
              <Label>DropdownSearchBar (disabled)</Label>
              <DropdownSearchBar id="dropdown-search-bar-1-dis" disabled placeholder="Type something" onSearchChanged={this.searchChanged}>
                <Container>
                  <Row>
                    <Col>
                      Custom content goes here:
                      <Button>Button</Button>
                      <CalendarInput id="expiry-date-cal-2-dis" value={this.state.date} onChange={this.handleDateChange} />
                    </Col>
                  </Row>
                </Container>
              </DropdownSearchBar>
            </Col>
          </Row>
          <Row>
            <Col>
              <Label>File Upload </Label>
              <FileUpload id="files" />
            </Col>
          </Row>
        </Card>
      </>
    );
  }
}

import React, { ReactNode } from 'react';
import { LoginForm } from 'lib/components';

interface ILoginExampleProps {
  children: ReactNode;
}

export class LoginExample extends React.Component<ILoginExampleProps> {
  render() {
    const onLoginClicked = () => {};
    return (
      <LoginForm
        title="Spoon-ui"
        onLoginClicked={onLoginClicked}
        titleUrl="content/images/SpoonUI_logo.svg"
        logoUrl="content/images/SpoonUI_icon.svg"
      />
    );
  }
}

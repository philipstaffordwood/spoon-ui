import React, { cloneElement, Children, ReactElement } from 'react';
import RPaginate from 'react-paginate';
import cx from 'classnames';
import { Row } from '../layout';
import { ItemsPerPage, IItemsPerPageState } from '../items-per-page/items-per-page';

export interface ICurrentPageData {
  data: any[];
  pageCount: number;
  offset: number;
}

export interface IPaginationProps {
  data: any[];
  initialPage: number;
  perPageDefault: number;
  // if this prop is not specified, it will be calculated based on data.length
  totalPageCount?: number;
  showPerPage?: boolean;
  justify?: 'start' | 'end' | 'center';
  onPageChange?: (selectedPage: number, offset: number, perPage?: number) => void;
  // render callback to use as a child
  render?: (pageData: ICurrentPageData) => ReactElement<any>;
}

export interface IPaginationState {
  currentData: any[];
  pageCount: number;
  currentPage: number;
  offset: number;
  perPage: number;
}

export class Pagination extends React.Component<IPaginationProps, IPaginationState> {
  static defaultProps: IPaginationProps = {
    data: [],
    onPageChange: (selectedPage, offset, perPage?) => {},
    initialPage: 0,
    perPageDefault: 10,
    showPerPage: false
  };

  constructor(props: IPaginationProps) {
    super(props);
    this.state = {
      perPage: this.props.perPageDefault,
      pageCount: this.props.totalPageCount ? this.props.totalPageCount : Math.ceil(props.data.length / this.props.perPageDefault),
      currentPage: props.initialPage,
      offset: this.props.perPageDefault * props.initialPage,
      currentData: []
    };
  }

  private setCurrentData() {
    const currentData = this.props.data.slice(this.state.offset, this.state.offset + this.state.perPage);
    this.setState({ currentData });
  }

  private handlePageChange = (page, perPage) => {
    const { onPageChange } = this.props;
    const { selected } = page;
    const offset = selected * perPage;
    this.setState({ currentPage: selected, offset }, this.setCurrentData);
    onPageChange && onPageChange(selected, offset, perPage);
  };

  componentDidMount() {
    this.setCurrentData();
  }

  onPerPageChange = value => {
    const pageCount = this.props.totalPageCount ? this.props.totalPageCount : Math.ceil(this.props.data.length / value);
    const offset = value * this.props.initialPage;
    const perPage = value;
    this.setState({ perPage, pageCount, offset });
    this.handlePageChange({ selected: 0 }, perPage);
  };

  render() {
    const { currentData, offset, pageCount } = this.state;
    const { children, justify } = this.props;
    const renderData = { data: currentData, offset, pageCount };
    const className = cx('pagination', {
      'justify-content-start': justify === 'start',
      'justify-content-end': justify === 'end',
      'justify-content-center': justify === 'center'
    });

    return (
      <>
        {this.props.render ? this.props.render(renderData) : null}
        {children ? cloneElement(Children.only(children as ReactElement<any>), renderData) : null}
        <Row justify="between">
          <RPaginate
            onPageChange={this.handlePageChange}
            pageCount={this.state.pageCount}
            forcePage={this.state.currentPage}
            containerClassName={className}
            pageClassName="page-item"
            pageLinkClassName="page-link"
            activeClassName="active"
            previousClassName="page-item"
            previousLinkClassName="page-link left"
            previousLabel=""
            nextClassName="page-item"
            nextLinkClassName="page-link right"
            nextLabel=""
            breakClassName="page-item"
            breakLinkClassName="page-link"
            breakLabel="⋯"
            extraAriaContext="⋯"
            marginPagesDisplayed="1"
          />
          {this.props.showPerPage && <ItemsPerPage defaultPerPage={this.state.perPage} onChange={this.onPerPageChange} />}
        </Row>
      </>
    );
  }
}

import './form-input.scss';
import React, { ChangeEvent } from 'react';
import cx from 'classnames';
import { IDirtyInput } from '../../util/dirty-input';
import { translateItem } from '../../util/translation';
import { Input, InputProps, Label } from 'reactstrap';
import Check from '@material-ui/icons/CheckRounded';
import ClearIcon from '@material-ui/icons/Clear';
import {
  IFormInput,
  IFormInputState,
  checkValidAndErrorState,
  handleFormDidUpdate,
  defaultFormInputState,
  formInputGroup
} from './form-input';

export interface ITextInputProps extends Omit<InputProps, 'value' | 'onChange' | 'placeholder'>, IDirtyInput<string>, IFormInput<string> {
  enableTicks?: boolean;
  required?: boolean;
  iconRight?: JSX.Element;
  iconLeft?: JSX.Element;
}

export class TextInput extends React.Component<ITextInputProps, IFormInputState<string>> {
  state: IFormInputState<string> = defaultFormInputState<string>();

  componentDidMount() {
    checkValidAndErrorState(this);
  }

  componentDidUpdate(prevProps: Readonly<ITextInputProps>, prevState: Readonly<IFormInputState<string>>, snapshot?: any) {
    handleFormDidUpdate(this, prevProps, prevState);
  }

  render() {
    const {
      id,
      label,
      className,
      onMadeDirty,
      onChange,
      placeholder,
      value,
      required,
      disabled,
      enableTicks,
      iconRight,
      iconLeft,
      ...other
    } = this.props;

    const onChangeDirty = (e: ChangeEvent<HTMLInputElement>) => {
      onChange(e.target.value);
      if (!!onMadeDirty) {
        onMadeDirty();
      }
    };
    const isInvalid = this.state.valid;
    const isValid = this.state.valid;
    const classIconRight = iconRight ? 'input-with-icon-right' : null;
    const classIconLeft = iconLeft ? 'input-with-icon-left' : null;

    const input = (
      <>
        <Input
          id={id}
          placeholder={translateItem(placeholder)}
          value={value}
          onChange={onChangeDirty}
          validAndDirty={isValid}
          invalid={isInvalid}
          className={cx(className, { required }, classIconRight, classIconLeft)}
          disabled={disabled}
          {...other}
        />
        {!!iconLeft ? <div className="icon-for-input left-icon">{iconLeft}</div> : null}
        {!!iconRight ? <div className="icon-for-input right-icon">{iconRight}</div> : null}
        {!isInvalid && enableTicks && !iconRight && <ClearIcon id="clear" className={`material-icons invalid-icon disabled-${disabled}`} />}
        {isValid && enableTicks && !iconRight && <Check id="check" className={`material-icons valid-icon disabled-${disabled}`} />}
      </>
    );
    return (
      <>
        {this.props.label && <Label className={disabled && 'disabled'}>{translateItem(this.props.label)}</Label>}
        {formInputGroup(this, input, this.props.required)}
      </>
    );
  }
}

import './alert.scss';

import React from 'react';
import { UncontrolledAlert, AlertProps } from 'reactstrap';

export interface IAlertProp extends AlertProps {
  color?: 'info' | 'warning' | 'danger' | 'success';
  secondary?: boolean;
}

export class Alert extends React.Component<IAlertProp> {
  render() {
    const { color = 'info', secondary = false, children, ...other } = this.props;
    return (
      <UncontrolledAlert color={color} className={secondary && 'secondary'} {...other}>
        <>
          <span className="alert-icon" />
          <div className="alert-text">{children}</div>
        </>
      </UncontrolledAlert>
    );
  }
}

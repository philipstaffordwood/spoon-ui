import React from 'react';
import { Col, Row } from 'reactstrap';
import { ColorVariableSetter, getRootColorVariable } from '../color-variable-setter/color-variable-setter';
import { translateItem } from '../../util/translation';

export interface IColorGroup {
  groupName: string;
  names: string[];
}

export interface IColorThemeEditorProps {
  groups: IColorGroup[];
}

export const colorsFlat: IColorGroup = {
  groupName: 'Flat Color',
  names: ['main-color', 'main-color-light', 'info', 'alert-info']
};

export const colorsAlerts: IColorGroup = {
  groupName: 'Messaging',
  names: ['success', 'alert-success', 'danger', 'alert-danger', 'warning', 'alert-warning']
};

export const colorsAllGroups = [colorsFlat, colorsAlerts];

export function downloadThemeScssFile(): void {
  const text =
    [].concat
      .apply([], colorsAllGroups.map(b => b.names))
      .map(n => `$${n}: ${getRootColorVariable(n)} !default;`)
      .join('\n') + "\n\n@import 'node_modules/@grindrodbank/spoon-ui/spoon-ui';\n";
  const blob: Blob = new Blob([text], { type: 'text/scss' });
  const fileName = 'theme.scss';
  const objectUrl: string = URL.createObjectURL(blob);
  const a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;

  a.href = objectUrl;
  a.download = fileName;
  document.body.appendChild(a);
  a.click();

  document.body.removeChild(a);
  URL.revokeObjectURL(objectUrl);
}

export class ColorThemeEditor extends React.Component<IColorThemeEditorProps, {}> {
  static defaultProps: IColorThemeEditorProps = { groups: colorsAllGroups };

  render() {
    const headerText = name => name && translateItem({ name, display: name, value: 0 });
    return (
      <>
        {!!this.props.groups &&
          this.props.groups.map(group => (
            <>
              <h2>{headerText(group.groupName)}</h2>
              <Row>
                {group.names.map(b => (
                  <Col key={b} md={2}>
                    <ColorVariableSetter variable={b} />
                  </Col>
                ))}
              </Row>
            </>
          ))}
        {}
      </>
    );
  }
}

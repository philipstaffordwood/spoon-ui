import React from 'react';
import { translateItem } from '../../../util';
import { ContextMenu } from '../../context-menu/context-menu';
import './account.scss';
import * as defaultUserIcon from '../../../../static/images/avatar.png';

export interface ILoginMenuProps {
  name: string;
  userIcon?: string;
  onLogout: () => void;
}

export class AccountMenu extends React.Component<ILoginMenuProps> {
  render() {
    const { name, userIcon, onLogout } = this.props;
    return (
      <>
        <span>
          {translateItem('global.menu.account.title')},<b> {name}</b>
        </span>
        <ContextMenu
          className="account-menu"
          items={[
            {
              labelText: translateItem('global.menu.account.logout'),
              onClick: onLogout
            }
          ]}
          mainComponent={<img className="user-icon" src={userIcon || defaultUserIcon} />}
        />
      </>
    );
  }
}
